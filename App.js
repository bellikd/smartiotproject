import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Keyboard, TouchableWithoutFeedback } from 'react-native';
import MapView, { Marker, Circle } from 'react-native-maps';
import { StatusBar } from 'expo-status-bar';

export default function App() {
    const [location, setLocation] = useState(null);
    const [radius, setRadius] = useState('100');

    const handleSetLocation = (event) => {
        const { latitude, longitude } = event.nativeEvent.coordinate;
        setLocation({ latitude, longitude });
    };

    const handleRadiusChange = (text) => {
        setRadius(text);
    };

    const handleSubmit = () => {
        console.log('Location:', location);
        console.log('Radius:', radius);
        // Add your code to submit the location and radius to your backend or cloud here
        Keyboard.dismiss(); // Dismiss the keyboard on submit
    };

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    initialRegion={{
                        latitude: 53.3498,
                        longitude: -6.2603,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    onPress={handleSetLocation}
                >
                    {location && <Marker coordinate={location} />}
                    {location && (
                        <Circle
                            center={location}
                            radius={parseInt(radius, 10)}
                            strokeColor="rgba(0,0,255,0.5)"
                            fillColor="rgba(0,0,255,0.2)"
                        />
                    )}
                </MapView>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        value={radius}
                        onChangeText={handleRadiusChange}
                        keyboardType="numeric"
                        placeholder="Enter radius in meters"
                        returnKeyType="done" // Adds a "done" button to the number pad
                    />
                    <Button title="Set Geofence" onPress={handleSubmit} />
                </View>
                <StatusBar style="auto" />
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    inputContainer: {
        position: 'absolute',
        bottom: 50,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10, // Add padding to ensure the input does not touch screen edges
    },
    input: {
        width: '80%', // Use a percentage of the screen width for better responsiveness
        height: 50, // Increase height for better touch area
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 10,
        paddingHorizontal: 10,
        backgroundColor: 'white', // Ensure visibility on top of the map
        borderRadius: 5, // Optional styling for rounded corners
    },
});
